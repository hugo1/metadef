/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "register/op_impl_registry.h"
#include "register/op_impl_registry_api.h"
#include "framework/common/debug/ge_log.h"
#include "graph/operator_factory_impl.h"
#include "register/shape_inference.h"
#include "op_impl_register_v2_impl.h"

namespace gert {
namespace {
void RegisterOpImplToRegistry(const OpImplRegisterV2Impl *rd) {
  if (rd == nullptr) {
    GELOGW("The register data is invalid, the impl is nullptr");
    return;
  }
  auto &funcs = OpImplRegistry::GetInstance().CreateOrGetOpImpl(rd->op_type);
  if (rd->functions.infer_shape != nullptr) {
    funcs.infer_shape = rd->functions.infer_shape;
  }
  if (rd->functions.infer_shape_range != nullptr) {
    funcs.infer_shape_range = rd->functions.infer_shape_range;
  }
  if (rd->functions.infer_datatype != nullptr) {
    funcs.infer_datatype = rd->functions.infer_datatype;
  }
  if (rd->functions.tiling != nullptr) {
    funcs.tiling = rd->functions.tiling;
    funcs.max_tiling_data_size = rd->functions.max_tiling_data_size;
  }
  if (rd->functions.inputs_dependency != 0U) {
    funcs.inputs_dependency = rd->functions.inputs_dependency;
  }
  if (rd->functions.tiling_parse != nullptr) {
    funcs.tiling_parse = rd->functions.tiling_parse;
    funcs.compile_info_creator = rd->functions.compile_info_creator;
    funcs.compile_info_deleter = rd->functions.compile_info_deleter;
  }
  if (rd->is_private_attr_registered) {
    funcs.private_attrs = rd->functions.private_attrs;
    funcs.unique_private_attrs = rd->functions.unique_private_attrs;
  }
}
}
#if !defined ONLY_COMPILE_OPEN_SRC && !defined OP_IMPL_REGISTRY_ENABLE
OpImplRegister::OpImplRegister(const char *op_type)
    : op_type_(op_type),
      functions_(OpImplRegistry::GetInstance().CreateOrGetOpImpl(op_type)) {
  functions_.private_attrs.clear();
  functions_.unique_private_attrs.clear();
}
#else
OpImplRegister::OpImplRegister(const char *op_type) : op_type_(op_type) {
  functions_.infer_shape = nullptr;
  functions_.infer_shape_range = nullptr;
  functions_.infer_datatype = nullptr;
  functions_.tiling = nullptr;
  functions_.tiling_parse = nullptr;
  functions_.compile_info_creator = nullptr;
  functions_.compile_info_deleter = nullptr;
  functions_.max_tiling_data_size = 0;
  functions_.inputs_dependency = 0;
  functions_.is_register = false;
  memset(reserved_, 0, sizeof(reserved_));
}
OpImplRegister::OpImplRegister(const OpImplRegister &other) {
  OpImplRegistry::GetInstance().RegisterOpImpl(other.op_type_, other.functions_);
}
#endif

OpImplRegister &OpImplRegister::InferShape(OpImplKernelRegistry::InferShapeKernelFunc infer_shape_func) {
#if defined ONLY_COMPILE_OPEN_SRC || defined OP_IMPL_REGISTRY_ENABLE
  functions_.is_register = true;
#endif
  functions_.infer_shape = infer_shape_func;
  return *this;
}

OpImplRegister &OpImplRegister::InferShapeRange(
    OpImplKernelRegistry::InferShapeRangeKernelFunc infer_shape_range_func) {
#if defined ONLY_COMPILE_OPEN_SRC || defined OP_IMPL_REGISTRY_ENABLE
  functions_.is_register = true;
#endif
  functions_.infer_shape_range = infer_shape_range_func;
  return *this;
}

OpImplRegister &OpImplRegister::InferDataType(OpImplKernelRegistry::InferDataTypeKernelFunc infer_datatype_func) {
#if defined ONLY_COMPILE_OPEN_SRC || defined OP_IMPL_REGISTRY_ENABLE
  functions_.is_register = true;
#endif
  functions_.infer_datatype = infer_datatype_func;
  return *this;
}
OpImplRegister &OpImplRegister::Tiling(OpImplKernelRegistry::TilingKernelFunc tiling_func,
                                       size_t max_tiling_data_size) {
#if defined ONLY_COMPILE_OPEN_SRC || defined OP_IMPL_REGISTRY_ENABLE
  functions_.is_register = true;
#endif
  functions_.tiling = tiling_func;
  functions_.max_tiling_data_size = max_tiling_data_size;
  return *this;
}
OpImplRegister &OpImplRegister::InputsDataDependency(std::initializer_list<int32_t> inputs) {
  functions_.inputs_dependency = 0;
  for (const auto index : inputs) {
    if (functions_.SetInputDataDependency(index) != ge::GRAPH_SUCCESS) {
      GELOGE(ge::FAILED, "Failed to set data dependency for node %s, the input index %d", op_type_, index);
      return *this;
    }
  }
#if defined ONLY_COMPILE_OPEN_SRC || defined OP_IMPL_REGISTRY_ENABLE
  functions_.is_register = true;
#endif
  return *this;
}

OpImplRegister &OpImplRegister::PrivateAttrImpl(const char *private_attr, ge::AnyValue private_attr_av) {
  if (private_attr == nullptr) {
    GELOGE(ge::FAILED, "Failed to set private attr name using nullptr!");
  } else if (strncmp(private_attr, "", 1U) == 0) {
    GELOGE(ge::FAILED, "Failed to set private attr name using empty string!");
  } else {
    if (functions_.unique_private_attrs.insert(private_attr).second) {
#if defined ONLY_COMPILE_OPEN_SRC || defined OP_IMPL_REGISTRY_ENABLE
      functions_.is_register = true;
#endif
      functions_.private_attrs.emplace_back(std::make_pair(private_attr, std::move(private_attr_av)));
    } else {
      GELOGE(ge::FAILED, "The private attr name: %s has already existed.", private_attr);
    }
  }
  return *this;
}
OpImplRegister &OpImplRegister::PrivateAttr(const char *private_attr) {
  static ge::AnyValue emptyPrivateAttrAV;
  return PrivateAttrImpl(private_attr, emptyPrivateAttrAV);
}
OpImplRegister &OpImplRegister::PrivateAttr(const char *private_attr, int64_t private_attr_val) {
  return PrivateAttrImpl(private_attr, ge::AnyValue::CreateFrom<int64_t>(private_attr_val));
}
OpImplRegister &OpImplRegister::PrivateAttr(const char *private_attr, const std::vector<int64_t> &private_attr_val) {
  return PrivateAttrImpl(private_attr, ge::AnyValue::CreateFrom<std::vector<int64_t>>(private_attr_val));
}
OpImplRegister &OpImplRegister::PrivateAttr(const char *private_attr, const char *private_attr_val) {
  return PrivateAttrImpl(private_attr, ge::AnyValue::CreateFrom<std::string>(private_attr_val));
}
OpImplRegister &OpImplRegister::PrivateAttr(const char *private_attr, float private_attr_val) {
  return PrivateAttrImpl(private_attr, ge::AnyValue::CreateFrom<float>(private_attr_val));
}
OpImplRegister &OpImplRegister::PrivateAttr(const char *private_attr, bool private_attr_val) {
  return PrivateAttrImpl(private_attr, ge::AnyValue::CreateFrom<bool>(private_attr_val));
}
OpImplRegister &OpImplRegister::PrivateAttr(const char *private_attr, const vector<float> &private_attr_val) {
  return PrivateAttrImpl(private_attr, ge::AnyValue::CreateFrom<std::vector<float>>(private_attr_val));
}
OpImplRegistry &OpImplRegistry::GetInstance() {
  static OpImplRegistry instance;
  return instance;
}

OpImplRegistry::OpImplFunctions &OpImplRegistry::CreateOrGetOpImpl(const OpImplRegistry::OpType &op_type) {
  return types_to_impl_[op_type];
}
const OpImplRegistry::OpImplFunctions *OpImplRegistry::GetOpImpl(const OpImplRegistry::OpType &op_type) const {
  auto iter = types_to_impl_.find(op_type);
  if (iter == types_to_impl_.end()) {
    return nullptr;
  }
  return &iter->second;
}
const OpImplRegistry::PrivateAttrList &OpImplRegistry::GetPrivateAttrs(const OpImplRegistry::OpType &op_type) const {
  auto op_impl_ptr = GetOpImpl(op_type);
  if (op_impl_ptr == nullptr) {
    static OpImplRegistry::PrivateAttrList emptyPrivateAttr;
    return emptyPrivateAttr;
  }
  return op_impl_ptr->private_attrs;
}
#if defined ONLY_COMPILE_OPEN_SRC || defined OP_IMPL_REGISTRY_ENABLE
void OpImplRegistry::RegisterOpImpl(const OpType &op_type, OpImplRegistry::OpImplFunctions func) {
  (void) reserved_;
  if (!func.is_register) {
      types_to_impl_[op_type] = func;
      return;
  }
  types_to_impl_[op_type].is_register = func.is_register;
  if (func.infer_shape != nullptr) {
    types_to_impl_[op_type].infer_shape= std::move(func.infer_shape);
  }
  if (func.infer_shape_range != nullptr) {
    types_to_impl_[op_type].infer_shape_range= std::move(func.infer_shape_range);
  }
  if (func.infer_datatype != nullptr) {
    types_to_impl_[op_type].infer_datatype= std::move(func.infer_datatype);
  }
  if (func.tiling != nullptr) {
    types_to_impl_[op_type].tiling= std::move(func.tiling);
  }
  if (func.tiling_parse != nullptr) {
    types_to_impl_[op_type].tiling_parse= std::move(func.tiling_parse);
  }
  if (func.compile_info_creator != nullptr) {
    types_to_impl_[op_type].compile_info_creator= std::move(func.compile_info_creator);
  }
  if (func.compile_info_deleter != nullptr) {
    types_to_impl_[op_type].compile_info_deleter= std::move(func.compile_info_deleter);
  }
  if (func.max_tiling_data_size != 0) {
    types_to_impl_[op_type].max_tiling_data_size= std::move(func.max_tiling_data_size);
  }
  if (func.inputs_dependency != 0) {
    types_to_impl_[op_type].inputs_dependency= std::move(func.inputs_dependency);
  }
  if (!func.private_attrs.empty()) {
    types_to_impl_[op_type].private_attrs= std::move(func.private_attrs);
  }
  if (!func.unique_private_attrs.empty()) {
    types_to_impl_[op_type].unique_private_attrs= std::move(func.unique_private_attrs);
  }
}
#endif
const std::map<OpImplRegistry::OpType, OpImplRegistry::OpImplFunctions> &OpImplRegistry::GetAllTypesToImpl() const {
  return types_to_impl_;
}
std::map<OpImplRegistry::OpType, OpImplRegistry::OpImplFunctions> &OpImplRegistry::GetAllTypesToImpl() {
  return types_to_impl_;
}
OpImplRegisterV2::OpImplRegisterV2(const ge::char_t *op_type) : impl_(new(std::nothrow) OpImplRegisterV2Impl) {
  if (impl_ != nullptr) {
    impl_->op_type = op_type;
    impl_->functions.infer_shape = nullptr;
    impl_->functions.infer_shape_range = nullptr;
    impl_->functions.infer_datatype = nullptr;
    impl_->functions.inputs_dependency = 0U;

    // two fields controlled by tiling func
    impl_->functions.tiling = nullptr;
    impl_->functions.max_tiling_data_size = std::numeric_limits<size_t>::max();

    // 3 fields controlled by tiling_parse func
    impl_->functions.tiling_parse = nullptr;
    impl_->functions.compile_info_creator = nullptr;
    impl_->functions.compile_info_deleter = nullptr;

    // private attr controlled by is_private_attr_registered

    OpImplRegistry::GetInstance().CreateOrGetOpImpl(op_type);
  }
}
OpImplRegisterV2::~OpImplRegisterV2() = default;
OpImplRegisterV2::OpImplRegisterV2(const OpImplRegisterV2 &register_data) {
  RegisterOpImplToRegistry(register_data.impl_.get());
}
OpImplRegisterV2::OpImplRegisterV2(OpImplRegisterV2 &&register_data) noexcept {
  RegisterOpImplToRegistry(register_data.impl_.get());
}
OpImplRegisterV2 &OpImplRegisterV2::TilingParse(KernelRegistry::KernelFunc tiling_parse_func,
                                                OpImplKernelRegistry::CompileInfoCreatorFunc creator_func,
                                                OpImplKernelRegistry::CompileInfoDeleterFunc deleter_func) {
  if (impl_ != nullptr) {
    impl_->functions.tiling_parse = tiling_parse_func;
    impl_->functions.compile_info_creator = creator_func;
    impl_->functions.compile_info_deleter = deleter_func;
  }
  return *this;
}
OpImplRegisterV2 &OpImplRegisterV2::InferShape(OpImplKernelRegistry::InferShapeKernelFunc infer_shape_func) {
  if (impl_ != nullptr) {
    impl_->functions.infer_shape = infer_shape_func;
  }
  return *this;
}
OpImplRegisterV2 &OpImplRegisterV2::InferShapeRange(
    OpImplKernelRegistry::InferShapeRangeKernelFunc infer_shape_range_func) {
  if (impl_ != nullptr) {
    impl_->functions.infer_shape_range = infer_shape_range_func;
  }
  return *this;
}
OpImplRegisterV2 &OpImplRegisterV2::InferDataType(OpImplKernelRegistry::InferDataTypeKernelFunc infer_datatype_func) {
  if (impl_ != nullptr) {
    impl_->functions.infer_datatype = infer_datatype_func;
  }
  return *this;
}
OpImplRegisterV2 &OpImplRegisterV2::Tiling(OpImplKernelRegistry::TilingKernelFunc tiling_func,
                                           size_t max_tiling_data_size) {
  if (impl_ != nullptr) {
    impl_->functions.tiling = tiling_func;
    impl_->functions.max_tiling_data_size = max_tiling_data_size;
  }
  return *this;
}
OpImplRegisterV2 &OpImplRegisterV2::InputsDataDependency(std::initializer_list<int32_t> inputs) {
  if (impl_ != nullptr) {
    impl_->functions.inputs_dependency = 0;
    for (const auto index : inputs) {
      if (impl_->functions.SetInputDataDependency(index) != ge::GRAPH_SUCCESS) {
        GELOGE(ge::FAILED, "Failed to set data dependency for node %s, the input index %d", impl_->op_type.c_str(),
               index);
        return *this;
      }
    }
  }
  return *this;
}
OpImplRegisterV2 &OpImplRegisterV2::PrivateAttr(const ge::char_t *private_attr, ge::AnyValue private_attr_av) {
  if (private_attr == nullptr) {
    GELOGW("Failed to set private attr name using nullptr!");
    return *this;
  }
  if (strncmp(private_attr, "", 1U) == 0) {
    GELOGW("Failed to set private attr name using empty string!");
    return *this;
  }
  if (impl_ != nullptr) {
    impl_->is_private_attr_registered = true;
    if (impl_->functions.unique_private_attrs.insert(private_attr).second) {
      impl_->functions.private_attrs.emplace_back(std::make_pair(private_attr, std::move(private_attr_av)));
    } else {
      GELOGW("The private attr name: %s has already existed.", private_attr);
    }
  }
  return *this;
}
OpImplRegisterV2 &OpImplRegisterV2::PrivateAttr(const ge::char_t *private_attr) {
  static ge::AnyValue empty;
  return PrivateAttr(private_attr, empty);
}
OpImplRegisterV2 &OpImplRegisterV2::PrivateAttr(const ge::char_t *private_attr, int64_t private_attr_val) {
  return PrivateAttr(private_attr, ge::AnyValue::CreateFrom<int64_t>(private_attr_val));
}
OpImplRegisterV2 &OpImplRegisterV2::PrivateAttr(const ge::char_t *private_attr,
                                                const vector<int64_t> &private_attr_val) {
  return PrivateAttr(private_attr, ge::AnyValue::CreateFrom<std::vector<int64_t>>(private_attr_val));
}
OpImplRegisterV2 &OpImplRegisterV2::PrivateAttr(const ge::char_t *private_attr, const ge::char_t *private_attr_val) {
  return PrivateAttr(private_attr, ge::AnyValue::CreateFrom<std::string>(private_attr_val));
}
OpImplRegisterV2 &OpImplRegisterV2::PrivateAttr(const ge::char_t *private_attr, float private_attr_val) {
  return PrivateAttr(private_attr, ge::AnyValue::CreateFrom<float>(private_attr_val));
}
OpImplRegisterV2 &OpImplRegisterV2::PrivateAttr(const ge::char_t *private_attr, bool private_attr_val) {
  return PrivateAttr(private_attr, ge::AnyValue::CreateFrom<bool>(private_attr_val));
}
OpImplRegisterV2 &OpImplRegisterV2::PrivateAttr(const ge::char_t *private_attr, const vector<float> &private_attr_val) {
  return PrivateAttr(private_attr, ge::AnyValue::CreateFrom<std::vector<float>>(private_attr_val));
}
}  // namespace gert

#ifdef __cplusplus
extern "C" {
#endif

size_t GetRegisteredOpNum(void) {
  return gert::OpImplRegistry::GetInstance().GetAllTypesToImpl().size();
}
int32_t GetOpImplFunctions(TypesToImpl *impl, size_t impl_num) {
  auto types_to_impl = gert::OpImplRegistry::GetInstance().GetAllTypesToImpl();
  if (impl_num != types_to_impl.size()) {
    GELOGE(ge::FAILED, "Get types_to_impl_ failed, impl_num[%u] and map size[%u] not match",
           impl_num, types_to_impl.size());
    return ge::GRAPH_FAILED;
  }
  size_t cnt = 0U;
  for (auto &it : types_to_impl) {
    impl[cnt].op_type = it.first.c_str();
    impl[cnt].funcs = it.second;
    cnt++;
  }
  return ge::GRAPH_SUCCESS;
}

#ifdef __cplusplus
}
#endif

